<?php
include_once 'index.php';
if (isset($_FILES['file'])) {
    $fileTmpName = $_FILES['file']['tmp_name'];
    $image = getimagesize($fileTmpName);
    $name = '1';
    $extension = image_type_to_extension($image[2]);
    $format = str_replace('jpeg', 'jpg', $extension);
    $pap = $_SESSION['stuff_id'];
    if (!move_uploaded_file($fileTmpName, __DIR__ . '/upload/' . $pap . '/' . $name . $format)) {
        die('При записи изображения на диск произошла ошибка.');
    }
    $_FILES['file'] = NULL;

    header('Location: http://localhost/katalog/');
    exit;
}
?>